from . import models

def tags(request):
    return {
        'tags': models.Tag.objects.hot().all()[0:10],
    }

def users(request):
    return {
        'users': models.User.objects.all()[0:10]
    }
def cur_page(request):
    page = -1

    try:
        if "page" in request.GET:
            page = int(request.GET["page"])
    except:
        page = -1

    return max(page, 1)

def pages(objects_list, request, per_page=10):
    last_page = (objects_list.count() - 1) // per_page + 1
    page = cur_page(request)
    min_page = max(page - 5, 1)
    max_page = min(min_page + 10, last_page)
    if max_page == 1:
        return None
    return range(min_page, max_page + 1) 

def paginate(objects_list, request, per_page=10):
    page = cur_page(request)
    
    data = objects_list[(page - 1) * per_page : page * per_page]

    return data
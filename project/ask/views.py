from django.http.response import Http404
from django.shortcuts import render
from .models import Question, Answer, User
from . import paginator
from django.db.models import Count

def index(request):
    data = Question.objects.with_answers_count().all()
    return render(request, "index.html", context={
        "category": "Old",
        "questions": paginator.paginate(data, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(data, request),
    })

def new(request):
    data = Question.objects.new()
    return render(request, "index.html", context={
        "category": "New",
        "questions": paginator.paginate(data, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(data, request),
    })

def hot(request):
    data = Question.objects.hot()
    return render(request, "index.html", context={
        "category": "Hot",
        "questions": paginator.paginate(data, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(data, request),
    })

def by_tag(request, tag):
    data = Question.objects.where_tag(tag)
    return render(request, "index.html", context={
        "category": "Tag " + tag,
        "questions": paginator.paginate(data, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(data, request),
    })

def by_user(request, user_id):
    data = Question.objects.where_user(user_id)
    user:User = User.objects.get(pk=user_id)
    
    return render(request, "index.html", context={
        "category": "User {:}".format(user.username),
        "questions": paginator.paginate(data, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(data, request),
    })


def question(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except Question.DoesNotExist:
        raise Http404
    
    answers = Answer.objects.hot().filter(question_id = question_id)
    return render(request, "question.html", {
        "question": question,
        "answers": paginator.paginate(answers, request),
        "page": paginator.cur_page(request),
        "pages": paginator.pages(answers, request),
    })

def signin(request):
    return render(request, "signin.html")

def signup(request):
    return render(request, "signup.html")

def ask(request):
    return render(request, "ask.html")

def settings(request):
    return render(request, "settings.html")
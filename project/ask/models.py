from django.db import models
from django.contrib.auth.models import User
from django.db.models.fields import IntegerField
from .reaction import Reaction, ReactionParentModel


class QuestionManager(models.Manager):
    def with_answers_count(self):
        return self.annotate(answers_count=models.Count('answer'))

    def where_tag(self, tag: str):
        return self.filter(tags__name=tag)

    def where_user(self, user_id: int):
        return self.filter(author_id=user_id)

    def new(self):
        return self.order_by("-date_create")

    def hot(self):
        return self.order_by("-rating")


class AnswerManager(models.Manager):
    def hot(self):
        from django.db.models import Q, F, ExpressionWrapper
        rating = (models.Count('reactionanswer', filter=Q(
                reactionanswer__reaction=Reaction.LIKE)) -
            models.Count('reactionanswer', filter=Q(
                reactionanswer__reaction=Reaction.DISLIKE)))
        return self\
            .annotate(rating_c=rating)\
            .order_by("-rating_c").all()            

class TagManager(models.Manager):
    def with_questions_count(self):
        return self.annotate(question_count=models.Count('question'))

    def hot(self):
        return self.with_questions_count().order_by("-question_count")


class Question(models.Model, ReactionParentModel):
    title = models.CharField(max_length=255, verbose_name="Заголовок")
    body = models.TextField(verbose_name="Текст вопроса")
    date_create = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Автор")
    tags = models.ManyToManyField('Tag', verbose_name="Теги")
    rating = models.IntegerField(verbose_name='Рейтинг', default=0)
    objects = QuestionManager()

    def count_answers(self):
        return Answer.objects.filter(question_id=self.pk).count()

    def reactions(self):
        return ReactionQuestion.objects.filter(question_id=self.id)

    class Meta:
        verbose_name = "Вопрос"
        verbose_name_plural = "Вопросы"


class Answer(models.Model, ReactionParentModel):
    body = models.TextField(verbose_name="Текст ответа")
    is_correct = models.BooleanField()
    date_create = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(
        'Question', on_delete=models.CASCADE, verbose_name="Вопрос")
    author = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name="Автор")

    objects = AnswerManager()

    def reactions(self):
        return ReactionAnswer.objects.filter(answer_id=self.id)

    class Meta:
        verbose_name = "Ответ"
        verbose_name_plural = "Ответы"


class Tag(models.Model):
    name = models.CharField(
        max_length=255, verbose_name="Название тега", unique=True)

    objects = TagManager()

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"


class Profile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name="Пользователь")
    photo = models.URLField(verbose_name="Фотография")

    class Meta:
        verbose_name = "Профиль"
        verbose_name_plural = "Профили"


class ReactionAnswer(Reaction, models.Model):
    reaction = Reaction.reaction
    user = Reaction.user
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)


class ReactionQuestion(Reaction, models.Model):
    reaction = Reaction.reaction
    user = Reaction.user
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

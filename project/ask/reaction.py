from django.db import models
from django.contrib.auth.models import User

class Reaction():
    LIKE = 'L'
    DISLIKE = 'D'


    user = models.ForeignKey(User, on_delete=models.CASCADE)
    
    REACTION_CHOICES = [
        (LIKE, 'Like'),
        (DISLIKE, 'Dislike')
    ]

    reaction = models.CharField(
        max_length=1,
        choices=REACTION_CHOICES
    )

class ReactionParentModel():
    def reactions():
        pass
    def count_score(self):
        return self.reactions().filter(reaction=Reaction.LIKE).count() - self.reactions().filter(reaction=Reaction.DISLIKE).count()

import os
from typing import List
from django.core.management.base import BaseCommand
from django.db.models.query import Prefetch
from project.ask import models, reaction
import random
import json


class Command(BaseCommand):
    help = 'Fill db by data'

    def add_arguments(self, parser):
        parser.add_argument(
            '-u',
            '--users',
            action='store',
            default=100,
            help='Количество генерируемых пользователей'
        )
        parser.add_argument(
            '-q',
            '--questions',
            action='store',
            default=100,
            help='Количество генерируемых вопросов'
        )

        parser.add_argument(
            '-a',
            '--answers',
            action='store',
            default=100,
            help='Количество генерируемых ответов'
        )

        parser.add_argument(
            '-t',
            '--tags',
            action='store',
            default=0,
            help='Количество генерируемых тегов'
        )

        parser.add_argument(
            '-r',
            '--rection',
            action='store',
            default=0,
            help='Количество генерируемых реакций'
        )
    dir = os.path.dirname(__file__)
    LOREM = json.load(open(dir + "/lorem.json"))
    NICKNAME_FRAGMS = json.load(open(dir + "/nickfrags.json"))
    TAGS_PARTS_LANGS = json.load(open(dir + "/tagsfrags_langs.json"))
    TAGS_CONTEXT = json.load(open(dir + "/tags_context.json"))

    def create_tag(self):
        t = None
        try:
            tag_name = random.choice(
                self.TAGS_PARTS_LANGS) + '-' + random.choice(self.TAGS_CONTEXT)
            t = models.Tag(name=tag_name)
            t.save()
        except:
            t = self.create_tag()
        return t

    def create_text(self, words=100):
        nl = random.randint(- words // 10, words // 10)
        text = ""
        for i in range(words + nl):
            text = text + random.choice(self.LOREM) + " "
        return text

    def create_user(self):
        nl = random.randint(5, 10)
        nickname = ""
        for i in range(nl):
            nickname = nickname + random.choice(self.NICKNAME_FRAGMS)
        u = models.User(username=nickname)
        u.save()
        return u

    QUSESTION_WORD = [
        "What is",
        "How",
        "How long",
        "Where find",
        "Help! Who try",
        "How do I make",
    ]
    QUSESTION_CONTEXT = [
        "getting undefined type error",
        "update popups",
        "charge a js page",
        "stop loop terminating",
        "fix error",
        "extract data",
        "collect information",
        "get varible",
        "get information",
        "load updates"
    ]
    QUSESTION_SUBCONTEXT = [
        "in js",
        "in TS",
        "using google",
        "in cpp",
        "in c++",
        "in c",
        "in django",
        "when update",
        "when create project",
        "in azure",
        "in bmstu",
        "in angular",
        "in vuejs",
        "in nodejs",
        "in python",
        "in python3",
        "using django orm"
    ]

    def create_question(self, users, tags):
        question_title = " ".join([
            random.choice(self.QUSESTION_WORD),
            random.choice(self.QUSESTION_CONTEXT),
            random.choice(self.QUSESTION_SUBCONTEXT)

        ]) + "?"
        question_body = self.create_text()
        user = random.choice(users)
        q = models.Question(title=question_title,
                            body=question_body, author_id=user)

        return q

    def create_answer(self, users, questions):
        body = self.create_text(words=25)
        user = random.choice(users)
        question = random.choice(questions).id
        is_correct = random.randint(0, 10) == 10
        a = models.Answer(body=body, author_id=user,
                          question_id=question, is_correct=is_correct)

        return a

    def create_reaction_question(self, users, questions):
        user_id = random.choice(users)
        question_id = random.choice(questions).id
        reaction = random.choice([
            models.ReactionQuestion.LIKE,
            models.ReactionQuestion.DISLIKE
        ])

        r = models.ReactionQuestion(
            user_id=user_id, reaction=reaction, question_id=question_id)

        return r

    def create_reaction_answer(self, users, answers):
        user_id = random.choice(users)
        answer_id = random.choice(answers).id
        reaction = random.choice([
            models.ReactionQuestion.LIKE,
            models.ReactionQuestion.DISLIKE
        ])

        r = models.ReactionAnswer(
            user_id=user_id, reaction=reaction, answer_id=answer_id)

        return r

    def fix_question_rating(self, questions: List[models.Question], bulk_size=1000):
        def fix_rating(question: models.Question):
            question.rating = question.count_score()
            return question

        for i in range(len(questions)):
            models.Question.objects.bulk_update(
                [
                    fix_rating(q) for q in questions[i * bulk_size:(i + 1) * bulk_size]
                ],
                ['rating']
            )
            print("fix_question_rating ", i * bulk_size)

        pass

    def handle(self, *args, **options):
        users_count = int(options['users'])
        question_count = int(options['questions'])
        answers_count = int(options['answers'])
        tags_count = int(options['tags'])
        reaction_count = int(options['rection'])

        for i in range(users_count):
            u = self.create_user()
            print("create_user ", u)

        users = list(map(lambda x: x.id, models.User.objects.all()))

        for i in range(tags_count):
            t = self.create_tag()
            print("create_tag ", t)
        tags = list(map(lambda x: x.id, models.Tag.objects.all()))

        bulk_size = 1000
        for i in range(question_count // bulk_size):
            models.Question.objects.bulk_create([
                self.create_question(users, tags) for i in range(bulk_size)
            ])
            print("create_questions",  i*bulk_size)

        questions = models.Question.objects.all()
        answers = models.Answer.objects.all()

        for i in range(answers_count // bulk_size):
            models.Answer.objects.bulk_create([
                self.create_answer(users, questions) for i in range(bulk_size)
            ])
            print("create_answers ", i*bulk_size)

        for i in range(reaction_count // bulk_size):
            models.ReactionQuestion.objects.bulk_create([
                self.create_reaction_question(users, questions) for i in range(bulk_size)
            ])
            print("create_reactions_question ", i*bulk_size)

        self.fix_question_rating(questions, bulk_size)

        for i in range(reaction_count // bulk_size):
            models.ReactionAnswer.objects.bulk_create([
                self.create_reaction_answer(users, answers) for i in range(bulk_size)
            ])
            print("create_reactions_answer ", i * bulk_size)

        for q in questions:
            tags_count = random.randint(1,2)
            tags_r = random.sample(tags, tags_count)

            for tag in tags_r:
                q.tags.add(tag)
            print("set_tags ", q)


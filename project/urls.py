"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
import re
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static, serve
from django.urls import path, include # new
from .ask import views

urlpatterns = [

    path('', views.index, name="home"),

    path('new', views.new, name="new"),
    path('hot', views.hot, name="hot"),
    path('tag/<str:tag>', views.by_tag, name="tag"),
    path('questions/user/<int:user_id>', views.by_user, name="user"),

    path('signin', views.signin, name="signin"),
    path('signup', views.signup, name="signup"),
    path('question/<int:question_id>', views.question, name="question"),
    path('ask', views.ask, name="ask"),
    path('settings', views.settings, name="settings"),
    path('admin/', admin.site.urls),
    # static("/path")
]
